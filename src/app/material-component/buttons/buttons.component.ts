import { Component } from '@angular/core';

@Component({
  selector: 'app-buttons',
  preserveWhitespaces: true,
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss']
})
export class ButtonsComponent {
  constructor() { }
}
