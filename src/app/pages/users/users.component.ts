import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar} from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ModifyUserDescriptionDialog } from './dialogs/modify-description/modify-description-dialog.component';
import { DeleteUserDialog } from './dialogs/delete/delete-user-dialog.component';
import { ConfigureUserLinkDialog } from './dialogs/configure-links/configure-user-link-dialog.component';

export interface User {
  lastName: string;
  firstName: string;
  email: string;
  employer: string;
  account: string;
}

@Component({
  selector: 'app-users',
  preserveWhitespaces: true,
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  displayedColumns: string[] = ['select', 'lastName', 'firstName', 'employer', 'actions'];
  dataSource: MatTableDataSource<User>;
  selection = new SelectionModel<User>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog, public snackBar: MatSnackBar) {
    this.dataSource = new MatTableDataSource(USERS);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  openDeleteDialog() {
    const dialogRef = this.dialog.open(DeleteUserDialog, {
      width: '450px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.snackBar.open("Administrateurs supprimés avec succès", '', {
        duration: 2000,
      });
    });
  }

  openDescriptionDialog() {
    const dialogRef = this.dialog.open(ModifyUserDescriptionDialog, {
      width: '450px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.snackBar.open("Description modifiée avec succès", '', {
        duration: 2000,
      });
    });
  }

  openLinkDialog() {
    const dialogRef = this.dialog.open(ConfigureUserLinkDialog, {
      width: '450px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.snackBar.open("Liens modifiés avec succès", '', {
        duration: 2000,
      });
    });
  }
}

const USERS:User[] = [
  {lastName: 'ABOUELFADL', firstName: 'Yassine', email: 'y.abouelfadl.ext@bouygues-construction.com', employer: 'BOUYGUES CONSTRUCTION IT', account : 'BYCN\Y.ABOUELFADL'},
  {lastName: 'BRENNE', firstName: 'Sylvain', email: 's.brenne@bouygues-construction.com', employer: 'BOUYGUES CONSTRUCTION IT', account : 'BYCN\S.BRENNE'},
  {lastName: 'CHEMIN', firstName: 'Dominique', email: 'd.chemin@bouygues-construction.com', employer: 'BOUYGUES CONSTRUCTION IT', account : 'BYCN\D.CHEMIN'},
  {lastName: 'HUMEAU', firstName: 'Xavier', email: 'x.humeau@bouygues-construction.com', employer: 'BOUYGUES CONSTRUCTION IT', account : 'BYCN\X.HUMEAU'},
  {lastName: 'ASTIER', firstName: 'Muriel', email: 'mu.astier@bouygues-construction.com', employer: 'BOUYGUES CONSTRUCTION IT', account : 'BYCN\MU.ASTIER'},
  {lastName: 'BRENNE', firstName: 'Sylvain', email: 's.brenne@bouygues-construction.com', employer: 'BOUYGUES CONSTRUCTION IT', account : 'BYCN\S.BRENNE'},
  {lastName: 'CHEMIN', firstName: 'Dominique', email: 'd.chemin@bouygues-construction.com', employer: 'BOUYGUES CONSTRUCTION IT', account : 'BYCN\D.CHEMIN'},
  {lastName: 'GOURBIN', firstName: 'Vincent', email: 'v.gourbin@bouygues-construction.com', employer: 'BOUYGUES CONSTRUCTION IT', account : 'BYCN\V.GOURBIN'},
  {lastName: 'HUMEAU', firstName: 'Xavier', email: 'x.humeau@bouygues-construction.com', employer: 'BOUYGUES CONSTRUCTION IT', account : 'BYCN\X.HUMEAU'}
];