import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

/**
 * @title Stepper overview
 */
@Component({
  selector: 'new-user.component',
  templateUrl: 'new-user.component.html',
})
export class NewUserComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  mailTemplate:string = `Bonjour,

  Vous êtes désormais membre du groupe <group_name>, qui vous donne accès à <group_description>.
  Vos droits seront actifs sous peu.
  
  Cordialement,
  
  <administrator_first_name> <administrator_last_name>
  
  GROUPIE est une application Bouygues Construction Information Technologies.  
  `;

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: [this.mailTemplate, Validators.required]
    });
  }

  onChange(event) {
    if(event.checked) {
      this.secondFormGroup.get('secondCtrl').enable();
    } else {
      this.secondFormGroup.get('secondCtrl').disable();
    }
  }
}