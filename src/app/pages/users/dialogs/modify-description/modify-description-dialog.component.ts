import { Component } from '@angular/core';

@Component({
    selector: 'modify-description-dialog',
    templateUrl: 'modify-description-dialog.html',
  })
  export class ModifyUserDescriptionDialog {}