import { Component } from '@angular/core';

@Component({
    selector: 'delete-user-dialog',
    templateUrl: 'delete-user-dialog.html',
  })
  export class DeleteUserDialog {}