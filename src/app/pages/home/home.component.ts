import { Component,OnInit, AfterViewInit, ViewChild } from '@angular/core';
import {FormControl} from '@angular/forms';
import {merge, Observable} from 'rxjs';  
import {MatPaginator, MatTableDataSource, MatDialog,MatSort} from '@angular/material';
import { RenameGroupsDialog } from './dialogs/rename-groups/rename-groups.component';

export interface Options {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{
  displayedColumns: string[] = ['date', 'identifiant','chantier', 'actions'];
  dsCartosSauve = new MatTableDataSource<Catrographie>(CARTOGRAPHIE_SAUVEGARDE);
  dsCartosDone= new MatTableDataSource<Catrographie>(CARTOGRAPHIE_TERMINE);
  searchMode: string = 'admins';
  groupeType: string = 'mine';
 
  @ViewChild('t1', {read: MatSort})sort: MatSort; 
  @ViewChild('paginator1', {read: MatPaginator})  paginator: MatPaginator;
  @ViewChild('t2', {read: MatSort})  sortDone: MatSort;
  @ViewChild('paginator2', {read: MatPaginator})  paginatorDone: MatPaginator;
  constructor(public dialog: MatDialog) {}

  myControl = new FormControl();
  users: string[] = ['Zakaria DRISSI', 'Xavier HUMEAU', 'John Doe'];
  filteredUsers: Observable<string[]>;

  ngOnInit() {
    this.dsCartosSauve.paginator = this.paginator; 
    this.dsCartosSauve.sort = this.sort; 
    this.dsCartosDone.paginator = this.paginatorDone;
    this.dsCartosDone.sort = this.sortDone;  
  }

  filterCartoSauve(filterValue: string,field : string) {
    this.dsCartosSauve.filterPredicate = (data: Catrographie, filter: string) => {
      if(field==='date')
      return data.date.indexOf(filter) != -1;
      if(field==='id')
      return data.id.indexOf(filter) != -1;
      if(field==='chantier')
      return data.chantier.indexOf(filter) != -1;
    };
    this.dsCartosSauve.filter = filterValue.trim().toLowerCase();
  }

  filterCartoFinal(filterValue: string,field : string) {
    this.dsCartosDone.filterPredicate = (data: Catrographie, filter: string) => {
      if(field==='date')
      return data.date.indexOf(filter) != -1;
      if(field==='id')
      return data.id.indexOf(filter) != -1;
      if(field==='chantier')
      return data.chantier.indexOf(filter) != -1;
    };
    this.dsCartosDone.filter = filterValue.trim().toLowerCase();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.users.filter(option => option.toLowerCase().includes(filterValue));
  }

  selectSearchMode(mode: string) {
    this.searchMode = mode;
  }

  selectGroupeType(type: string) {
    this.groupeType = type;
  }

  openDialog() {
    const dialogRef = this.dialog.open(RenameGroupsDialog, {
      width: '510px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}

export interface Catrographie {
  date: string;
  identifiant: string;
  chantier: string;
}

const CARTOGRAPHIE_SAUVEGARDE: Catrographie[] = [
  { date : '05/10/2018', identifiant: 'm.ziane',chantier:'Chantier-DP102-LTA-320-90'},  
  { date : '06/10/2018', identifiant: 'a.bahrar',chantier:'Chantier-DP102-LTA-320-90'},  
  { date : '07/10/2018', identifiant: 'a.betayaa',chantier:'Chantier-DP102-LTA-320-90'},
  { date : '07/10/2018', identifiant: 'y.alin',chantier:'Chantier-DP102-LTA-320-90'} 
];

const CARTOGRAPHIE_TERMINE: Catrographie[] = [
  { date : '05/10/2018', identifiant: 'm.ziane',chantier:'Chantier-DP102-LTA-320-90'},  
  { date : '06/10/2018', identifiant: 'a.bahrar',chantier:'Chantier-DP102-LTA-320-90'},
  { date : '07/10/2018', identifiant: 'y.alin',chantier:'Chantier-DP102-LTA-320-90'},  
  { date : '08/10/2018', identifiant: 'a.betayaa',chantier:'Chantier-DP102-LTA-320-90'},
  { date : '09/10/2018', identifiant: 'm.ziane',chantier:'Chantier-DP102-LTA-320-90'},  
  { date : '10/10/2018', identifiant: 'a.bahrar',chantier:'Chantier-DP102-LTA-320-90'},
  { date : '11/10/2018', identifiant: 'y.alin',chantier:'Chantier-DP102-LTA-320-90'},  
  { date : '12/10/2018', identifiant: 'a.betayaa',chantier:'Chantier-DP102-LTA-320-90'},  
  { date : '13/10/2018', identifiant: 'a.bahrar',chantier:'Chantier-DP102-LTA-320-90'} 
];
