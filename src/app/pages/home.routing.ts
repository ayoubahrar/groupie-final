import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AdminsComponent } from './admins/admins.component';
import { NewAdminComponent } from './admins/new-admin/new-admin.component';
import { NewUserComponent } from './users/new-user/new-user.component';
import { UsersComponent } from './users/users.component';
import { ImportAdminComponent } from './admins/import-list/import-list.component';


export const HomeRoutes: Routes = [
  {
    path: 'administrateurs',
    component: AdminsComponent
  },
  {
    path: 'administrateurs/ajouter',
    component: NewAdminComponent
  },
  {
    path: 'administrateurs/importer',
    component: ImportAdminComponent
  },
  {
    path: 'utilisateurs',
    component: UsersComponent
  },
  {
    path: 'utilisateurs/ajouter',
    component: NewUserComponent
  },
  {
    path: 'home',
    component: HomeComponent
  }];
