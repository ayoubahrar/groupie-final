import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatRadioChange } from '@angular/material';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

/**
 * @title Stepper overview
 */
@Component({
  selector: 'import-list',
  templateUrl: 'import-list.component.html',
  styleUrls: ['import-list.component.scss'],
})
export class ImportAdminComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  displayedColumns = ['name'];
  dataSource = [];
  dataSource2 = OLD_MEMBERS;
  isLoadingMembers:boolean = false;
  myObservable = of({}).pipe(delay(1000));
  fileName: string = '';

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      importType: ['', Validators.required]
    });
  }

  search() {
    this.isLoadingMembers = true;
    this.myObservable.subscribe(() => {
      this.dataSource = ELEMENT_DATA;
      this.isLoadingMembers = false;      
    });
  } 

  onActionChange(event: MatRadioChange) {
    let filtered = ELEMENT_DATA.filter(e => e.valid);

    if (event.value == 'replace') {
      this.dataSource2 = filtered;
    } else if(event.value == 'add') {
      this.dataSource2 = OLD_MEMBERS.concat(filtered);
    }
  }

  fileEvent(event) {
    const fileSelected: File = event.target.files[0];
    this.fileName = fileSelected.name;
    this.search();
  }
}

export interface User {
  name: string;
  valid: boolean;
  new: boolean;
}

const ELEMENT_DATA: User[] = [
  {name : "ANTOINE Julien", valid: true, new: true},
  {name : "BAYEUX Olivier", valid: true, new: true},
  {name : "BENHAMIDA Adel", valid: true, new: true},
  {name : "DELIENNE Xavier", valid: true, new: true},
  {name : "DUMETZ Jean-Marie", valid: true, new: true},
  {name : "FERRIES Céline", valid: true, new: true},
  {name : "GALA Catherine", valid: true, new: true},
  {name : "GUEBLE Céline", valid: true, new: true},
  {name : "ISACESCO Laëtitia", valid: false, new: true},
  {name : "JOGUET Sandie", valid: true, new: true},
  {name : "KIENTZ Agnès", valid: false, new: true},
  {name : "KLEE Damien", valid: true, new: true},
  {name : "LOURY Dominique", valid: false, new: true},
  {name : "MALO-SCHWEBEL Christiane", valid: true, new: true},
  {name : "MARTIN Emmanuelle", valid: true, new: true},
  {name : "N'GUYEN Sophie", valid: true, new: true},
  {name : "PAISANT Stéphane", valid: true, new: true},
  {name : "PETIT Wylliam", valid: true, new: true},
  {name : "SOULAS Annie", valid: true, new: true},
];

const OLD_MEMBERS: User[] = [
  {name : "DRISSI Zakaria", valid: true, new: false},
  {name : "HUMEAU Xavier", valid: true, new: false},
];