import { Component } from '@angular/core';

@Component({
    selector: 'delete-admin-dialog',
    templateUrl: 'delete-admin-dialog.html',
  })
  export class DeleteAdminDialog {}