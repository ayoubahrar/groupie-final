import { Component } from '@angular/core';

@Component({
    selector: 'configure-admin-link-dialog',
    templateUrl: 'configure-admin-link-dialog.html',
  })
  export class ConfigureAdminLinkDialog {}