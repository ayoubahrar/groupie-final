import 'rxjs/add/operator/filter';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit
} from '@angular/core';


/** @title Responsive sidenav */
@Component({
  selector: 'app-full-layout',
  templateUrl: 'full.component.html',
  styleUrls: []
})
export class FullComponent implements OnDestroy, AfterViewInit {
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  breadcrumbs: any;
  labels: string[] = [];

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    for (let mapping of URL_MAPPING) {
      this.labels[mapping.path] = mapping.label;
    }

    this.breadcrumbLoader();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  breadcrumbLoader() {
    this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event) => {
      let paths: string[] = this.router.url.split('/');
      this.breadcrumbs = [];
      let currentPath = '';

      paths.forEach(path => {
        if (path == 'home')
          return;

        currentPath += path + '/';
        this.breadcrumbs.push({
          label: this.labels[path],
          url: currentPath
        });
      });
    });
  }

  ngAfterViewInit() { }
}


const URL_MAPPING: any[] = [
  { path: '', label: 'Accueil' },
  { path: 'home', label: 'Accueil' },
  { path: 'administrateurs', label: 'Gestion des administrateurs' },
  { path: 'utilisateurs', label: 'Gestion des utilisateurs' },
  { path: 'ajouter', label: 'Nouveau' },
  { path: 'importer', label: 'Importer des membres' },
];
